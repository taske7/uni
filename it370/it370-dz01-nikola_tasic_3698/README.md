 <div align="center">
 
 ![logo](https://www.metropolitan.ac.rs/files/2018/11/logo-01.png) 

 </div>

 <div align="center">
 
## Prolećni semetar, 2019/20

## *IT370: INTERAKCIJA ČOVEK-RAČUNAR*


## Domaći zadatak: 01

</div>

Ime i prezime: **Nikola Tasić**

Broj indeksa: **3698**

Datum izrade: **27.03.2020**

---

### <span style="color:#a70432">Tekst domaćeg zadatka:</span>

1. Prvi Deo - kognitivna analiza

    • Šta mislite šta je kognitivna analiza- opišite je i analizirajte i objasnite kako može biti korisna u domenu unapređivanja e-learning platforma Univerziteta Metropolitan.

    • Sprovedite task analizu (korišćenjem jedne od metoda koje ste vežbali u prethodnoj lekciji – posmatranje, nestrukturisani intervjui ) Navedite podatke o intervjuisanim/posmatranim osobama i priložite beleške.) Pokušajte da intervjuišete studente sa različitih fakulteta ili grupa. Posmatrajte upotrebu sistema na mobilnim uređajima. Podaci ne treba da budu previše privatni npr: Ivana, 22 godine studentkinja Grafičkog dizajna
    
    • Na osnovu dobijenih podataka izvedite zaključke - šta bi pomoglo unapređenju e-learning sistema.

2. Drugi deo – asistivne tehnologije

    Pokušajte da sprovedete task analizu na 2-3 osobe i samostalno, u otežanim uslovima – korišćenjem govornog interfejsa na OS, korišćenjem isključivo tastature za unos podataka i navigaciju na e-learning/estudent sistemu kao i na zimbri. U uvodu navedite uslove koje ispitujete, zatim opišite testiranje – 1-2 stranice, i izvedite zaključke / preporuke za unapređenje sistema 1 stranica. Kako biste sistem učinili dostupnijim studentima sa hendikepom. Vodite računa da treba da simulirate situacije u kojima je otežano korišćenje računara – zbog povreda ruku, vida, sluha , disfunkcionalnih uređaja (miš je van upotrebe), hladnoće …itd. Možete dodati i fotografije ili skice

### <span style="color:#a70432">Rešenje zadatka:</span>

    

